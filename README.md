### The allsource analytics is built using the following stack
* The codebase is in Node.js + Express
* The database is an array of mongodb instances
* docker is used for running the separate instances of the scripts for gathering/processing data 

### The Data - Main data structures:
* Contracts
* Wallets
* Transactions

### The scripts in the project can be eitSer single-instance or multi-instance and are responsible for the following tasks:
* (Single) Getting latest finalised block (every 32 blocks)
* (Multi) From the list of not-scanned blocks, request a block number, fetch all transactions for that block and forward to the database for storage
* (single) When a transaction is received on the server, if the contract is new its added to the **contracts** database
* (multi) For each contract every few hours the current floor price is updated, using the OpenSea API
* (multi) Each transaction is processed to calculate the following data:
    * Contracts Specific Stats
    * Wallet Specific Stats
